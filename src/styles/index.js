import {StyleSheet, Dimensions, Platform} from 'react-native';

const {height} = Dimensions.get('window');
const {width} = Dimensions.get('window');

export default StyleSheet.create({
  homeContainer: {
    flex: 1,
    flexDirection: 'column',
  },

  bigCardContainer: {
    width: width,
    backgroundColor: 'white',
    elevation: 6,
    marginBottom: 10,
    flexDirection: 'column',
  },

  bigCardImage: {
    width: width,
    height: height * 0.3,
  },

  bigCardContentContainer: {
    marginTop: 12,
  },

  bigCardTitle: {
    fontSize: 20,
    color: 'black',
    marginBottom: 16,
    marginStart: 16,
    paddingEnd: 16,
  },

  cardContainer: {
    width: width,
    backgroundColor: 'white',
    elevation: 6,
    flexDirection: 'column',
    marginBottom: 10,
  },

  cardImage: {
    width: width * 0.25,
    height: width * 0.25,
    position: 'absolute',
    end: 0,
    marginEnd: 16,
    marginBottom: 8,
  },

  cardContentContainer: {
    marginStart: 16,
    marginTop: 20,
    paddingBottom: 12,
    flexDirection: 'row',
  },

  cardImagesContainer: {
    marginTop: 12,
    marginStart: 16,
    flexDirection: 'row',
  },

  cardTitle: {
    width: width * 0.58,
    fontSize: 20,
    color: 'black',
    minHeight: 100,
  },

  cardCategory: {
    fontSize: 14,
    color: '#6B9DF1',
    fontWeight: '700',
    marginBottom: 35,
  },

  cardTimeTag: {
    color: '#6B7278',
  },

  shareIcon: {
    justifyContent: 'flex-end',
    position: 'absolute',
    marginTop: -8,
    end: 0,
    marginEnd: 18,
  },

  bookmarkIcon: {
    justifyContent: 'flex-end',
    position: 'absolute',
    marginTop: -8,
    end: 0,
    marginEnd: 80,
  },

  detailContainer: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'white',
  },

  detailScrollView: {
    paddingStart: 0,
    paddingEnd: 16,
    marginStart: 16,
  },

  detailTitle: {
    fontSize: 32,
    marginTop: 32,
    color: 'black',
  },

  detailFeaturedImage: {
    width: width - 32,
    height: height * 0.3,
    marginTop: 12,
    padding: 16,
    flexDirection: 'column',
    alignSelf: 'center',
  },

  detailEmphasizedText: {
    marginTop: 16,
    fontSize: 15,
    fontWeight: '700',
    color: 'black',
    letterSpacing: 0.15,
  },

  detailTextBody: {
    marginTop: 16,
    fontSize: 16,
    fontWeight: '700',
    color: 'black',
    letterSpacing: 0.1,
  },

  detailFeaturedCaption: {
    fontSize: 12,
    position: 'absolute',
    bottom: 0,
    paddingBottom: 8,
    paddingTop: 8,
    paddingEnd: 10,
    paddingStart: 10,
    borderTopRightRadius: 6,
    color: 'white',
    backgroundColor: 'rgba(0, 0, 0, 0.45)',
  },

  detailCategoryText: {
    fontSize: 14,
    color: '#6B9DF1',
    fontWeight: '700',
    marginBottom: 15,
    marginTop: 15,
  },

  detailTimeTag: {
    color: '#6B7278',
    fontWeight: '400',
  },

  backButton: {
    marginTop: 16,
    marginBottom: 16,
    marginStart: 30,
  },

  detailBottomBar: {
    elevation: 50,
    flexDirection: 'row',
    borderTopColor: 'grey',
    borderTopWidth: 0.2,
    width: width,
    backgroundColor: 'white',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
  },

  bookmarkIconDetail: {
    justifyContent: 'flex-end',
    position: 'absolute',
    marginTop: 16,
    end: 0,
    marginEnd: 100,
  },

  shareIconDetail: {
    justifyContent: 'flex-end',
    position: 'absolute',
    marginTop: 16,
    end: 0,
    marginEnd: 30,
  },

  tagTextItem: {
    fontSize: 12,
    borderColor: '#471629',
    borderWidth: 0.5,
    borderRadius: 18,
    marginEnd: 8,
    paddingHorizontal: 16,
    paddingVertical: 10,
    fontWeight: '600',
  },
  tagsFlatList: {
    marginTop: 12,
    marginBottom: 40,
  },
  detailTagsPlaceholder: {
    marginTop: 24,
    fontSize: 16,
    color: 'black',
    fontWeight: '700',
  },
});
