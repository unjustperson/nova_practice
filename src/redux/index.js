import articleReducer from '../screens/Detail/reducers';
import newsReducer from '../screens/Home/reducers';
import {combineReducers} from 'redux';

export default combineReducers({
  news: newsReducer,
  article: articleReducer,
});
