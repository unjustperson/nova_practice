import React from 'react';
import {View, Text} from 'react-native';
import {SvgXml} from 'react-native-svg';
import styles from '../../styles';
import shareXml from '../../assets/xml/shareXml';
import bookmarkXml from '../../assets/xml/bookmarkXml';

export default CardInfoBottom = ({data}) => {
  return (
    <View style={styles.cardImagesContainer}>
      <Text style={styles.cardCategory}>
        {typeof data.category_name === 'string'
          ? data.category_name.toUpperCase()
          : ''}
        <Text style={styles.cardTimeTag}> | {data.date_parsed}</Text>
      </Text>
      <SvgXml
        width={35}
        height={35}
        style={styles.bookmarkIcon}
        xml={bookmarkXml}
      />
      <SvgXml width={35} height={35} style={styles.shareIcon} xml={shareXml} />
    </View>
  );
};
