import React from 'react';
import {View, Text, Image, ImageBackground} from 'react-native';
import styles from '../../styles';
import CardInfoBottom from '../CardInfoBottom';
import {useNavigation} from '@react-navigation/native';
import {TouchableOpacity} from 'react-native-gesture-handler';

export default Card = ({data}) => {
  const navigation = useNavigation();

  return (
    <TouchableOpacity
      style={styles.cardContainer}
      onPress={() => navigation.navigate('Detail', {id: data.id})}>
      <View style={styles.cardContentContainer}>
        <Text style={styles.cardTitle}>{data.title}</Text>
        <Image style={styles.cardImage} source={{uri: data.thumbnail}} />
      </View>
      <CardInfoBottom data={data} />
    </TouchableOpacity>
  );
};
