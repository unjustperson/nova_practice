import React from 'react';
import {View, Text, Image} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import styles from '../../styles';
import {useNavigation} from '@react-navigation/native';
import CardInfoBottom from '../CardInfoBottom';

export default BigCard = ({data}) => {
  const navigation = useNavigation();

  return (
    <TouchableOpacity
      style={styles.bigCardContainer}
      onPress={() => navigation.navigate('Detail', {id: data.id})}>
      <Image style={styles.bigCardImage} source={{uri: data.thumbnail}} />
      <View style={styles.bigCardContentContainer}>
        <Text style={styles.bigCardTitle}>{data.title}</Text>
        <CardInfoBottom data={data} />
      </View>
    </TouchableOpacity>
  );
};
