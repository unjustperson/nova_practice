const shareXml = `
<svg width="28" height="28" viewBox="0 0 28 28" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M16.3333 10.5V5.83331L24.5 14L16.3333 22.1666V17.3833C10.5 17.3833 6.41667 19.25 3.5 23.3333C4.66667 17.5 8.16667 11.6666 16.3333 10.5Z" fill="#6B7278"/>
</svg>
`;

export default shareXml;
