const INITIAL_ARTICLE = {
  title: {
    rendered: '',
  },
  category_name: '',
  date_parsed: '',
  time_parsed: '',
  featured_image: '',
  featured_caption: '',
  acf: {
    'single-post_emphasized-text_text-area': '',
    'single-post_post-content_wysiwyg-editor': '',
  },
  tags: [
    {
      id: 0,
      name: '',
    },
  ],
};

const articleReducer = (state = INITIAL_ARTICLE, action) => {
  switch (action.type) {
    case 'GET_ARTICLE_JSON':
      const newArticleJson = action.payload;
      return newArticleJson;
    default:
      return state;
  }
};

export default articleReducer;
