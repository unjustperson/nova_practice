export const fetchArticleJson = (articleJson) => ({
  type: 'GET_ARTICLE_JSON',
  payload: articleJson,
});
