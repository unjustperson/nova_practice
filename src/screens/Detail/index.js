import {useNavigation} from '@react-navigation/native';
import React, {useState, useEffect} from 'react';
import {View, Text, Image, ImageBackground, Dimensions} from 'react-native';
import {FlatList, ScrollView} from 'react-native-gesture-handler';
import RenderHTML from 'react-native-render-html';
import {SafeAreaView} from 'react-native-safe-area-context';
import {SvgXml} from 'react-native-svg';
import {useDispatch, useSelector} from 'react-redux';
import backXml from '../../assets/xml/backXml';
import styles from '../../styles';
import bookmarkXml from '../../assets/xml/bookmarkXml';
import shareXml from '../../assets/xml/shareXml';
import {fetchArticleJson} from '../Detail/actions';

export default Detail = ({route}) => {
  const [isLoading, setLoading] = useState(true);
  const [botomBarVisible, setBottomBarVisible] = useState(true);
  const [scrollPositionY, setScrollPoistionY] = useState(0);

  const article = useSelector((state) => state.article);
  const dispatch = useDispatch();
  const navigation = useNavigation();

  const getArticle = async () => {
    try {
      const response = await fetch(
        `https://nova.rs/wp-json/wp/v2/posts/${route.params.id}`,
        {
          method: 'GET',
        },
      );
      const json = await response.json();
      const dataDetail = json;
      const articleJson = json;
      dispatch(fetchArticleJson(articleJson));
      {
        console.log(
          `articleTitle: ${
            article.title.rendered ? article.title.rendered : ''
          }`,
        );
      }
    } catch (error) {
      console.log(error);
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    getArticle();
  }, []);

  return article ? (
    <SafeAreaView
      style={styles.detailContainer}
      edges={['top', 'left', 'right']}>
      <ScrollView
        contentContainerStyle={styles.detailScrollView}
        onMomentumScrollBegin={(event) => {
          setScrollPoistionY(event.nativeEvent.contentOffset.y);
        }}
        onMomentumScrollEnd={(event) => {
          event.nativeEvent.contentOffset.y > scrollPositionY
            ? setBottomBarVisible(false)
            : setBottomBarVisible(true);
          // console.log(`offsetY ${event.nativeEvent.contentOffset.y}`);
          // console.log(`scrollY ${scrollPositionY}`);
        }}>
        <Text style={styles.detailTitle}>{article.title.rendered}</Text>
        <Text style={styles.detailCategoryText}>
          {article.category_name.toUpperCase()}
          <Text style={styles.detailTimeTag}>
            {' '}
            | {article.date_parsed} {article.time_parsed}
          </Text>
        </Text>
        <ImageBackground
          style={styles.detailFeaturedImage}
          source={{uri: article.featured_image}}>
          <Text style={styles.detailFeaturedCaption}>
            {article.featured_caption}
          </Text>
        </ImageBackground>
        <Text style={styles.detailEmphasizedText}>
          {article.acf['single-post_emphasized-text_text-area']}
        </Text>
        <RenderHTML
          style={styles.detailTextBody}
          ignoredDomTags={['img']}
          contentWidth={Dimensions.get('window') * 0.9}
          customHTMLElementModels="none"
          source={{
            html: article.acf['single-post_post-content_wysiwyg-editor'],
          }}></RenderHTML>
        <Text style={styles.detailTagsPlaceholder}>Tagovi</Text>
        <FlatList
          contentContainerStyle={styles.tagsFlatList}
          horizontal
          data={article.tags}
          renderItem={({item}) => (
            <Text style={styles.tagTextItem}>{item.name.toUpperCase()}</Text>
          )}
        />
      </ScrollView>
      <View
        style={[
          styles.detailBottomBar,
          botomBarVisible ? {display: 'flex'} : {display: 'none'},
        ]}>
        <SvgXml
          width={35}
          height={35}
          style={styles.backButton}
          xml={backXml}
          onPress={() => {
            navigation.goBack();
          }}
        />
        <SvgXml
          width={35}
          height={35}
          style={styles.bookmarkIconDetail}
          xml={bookmarkXml}
        />
        <SvgXml
          width={35}
          height={35}
          style={styles.shareIconDetail}
          xml={shareXml}
        />
      </View>
    </SafeAreaView>
  ) : (
    <Text> Error </Text>
  );
};
