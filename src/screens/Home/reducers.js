const INITIAL_NEWS = {
  firstHalf: [],
  secondHalf: [],
};

const newsReducer = (state = INITIAL_NEWS, action) => {
  switch (action.type) {
    case 'GET_NEWS':
      let {firstHalf, secondHalf} = state;
      firstHalf = action.payload;
      secondHalf = firstHalf.splice(4);
      const newState = {firstHalf, secondHalf};
      return newState;
    default:
      return state;
  }
};

export default newsReducer;
