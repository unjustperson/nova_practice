export const fetchNews = (dataFetched) => ({
  type: 'GET_NEWS',
  payload: dataFetched,
});
