import React, {useState, useEffect} from 'react';
import {FlatList} from 'react-native-gesture-handler';
import {SafeAreaView} from 'react-native-safe-area-context';
import {useDispatch, useSelector} from 'react-redux';
import BigCard from '../../components/BigCard';
import Card from '../../components/Card';
import styles from '../../styles';
import {useNavigation} from '@react-navigation/native';
import {fetchNews} from './actions';

export default Home = () => {
  const [isLoading, setLoading] = useState(true);

  const news = useSelector((state) => state.news);
  const dispatch = useDispatch();
  const navigation = useNavigation();

  const getNews = async () => {
    try {
      const response = await fetch(
        'https://nova.rs/wp-json/wp/v2/pages/1773229',
        {
          method: 'GET',
        },
      );
      const json = await response.json();
      const dataFetched = json.acf.featured_zone[0].category_repeater;
      dispatch(fetchNews(dataFetched));
    } catch (error) {
      console.log(error);
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    getNews();
  }, []);

  const shiftedData = (data) => {
    const newData = data.slice(1);
    return newData;
  };

  return (
    <SafeAreaView style={styles.homeContainer} edges={['top', 'left', 'right']}>
      <FlatList
        removeClippedSubviews={true}
        ListHeaderComponent={
          <BigCard
            data={news.firstHalf[0] ? news.firstHalf[0].category_post : {}}
          />
        }
        contentContainerStyle={styles.flatList}
        data={shiftedData(news.firstHalf) ? shiftedData(news.firstHalf) : {}}
        renderItem={({item}) => <Card data={item.category_post} />}
        ListFooterComponent={
          <FlatList
            removeClippedSubviews={true}
            ListHeaderComponent={
              <BigCard
                data={
                  news.secondHalf[0] ? news.secondHalf[0].category_post : {}
                }
              />
            }
            contentContainerStyle={styles.flatList}
            data={
              shiftedData(news.secondHalf) ? shiftedData(news.secondHalf) : {}
            }
            renderItem={({item}) => <Card data={item.category_post} />}
          />
        }
      />
    </SafeAreaView>
  );
};
